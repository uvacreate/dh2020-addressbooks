# DH2020 Address Books

Repository accompanying our paper [“Serving the city: an automatic information extraction for mapping Amsterdam nightlife (1820-1940)”](https://dh2020.adho.org/wp-content/uploads/2020/07/541_ServingthecityanautomaticinformationextractionformappingAmsterdamnightlife18201940.html), presented at DH2020 (virtual), 20-24 July 2020.  

## Presentation

[![Title page of the presentation](presentation.png)Download presentation (pdf)](https://gitlab.com/uvacreate/dh2020-addressbooks/-/raw/master/DH2020_UvA_Serving_the_City_presentation.pdf?inline=false)

## Contact

[createlab@uva.nl](mailto:createlab@uva.nl)